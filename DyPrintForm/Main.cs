﻿using System;
using System.Collections;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DyPrintForm
{
    public partial class Main : Form
    {
        //打印的任务队列
        Queue q=new Queue();
        //插队的任务队列
        Queue qq=new Queue();
        public Main()
        {
            InitializeComponent();
        }

        private async void Main_Load(object sender, EventArgs e)
        {
            await WebPrint.EnsureCoreWebView2Async(null);
            string absolutePath = System.IO.Path.GetFullPath("./Web/Print.html");
            WebPrint.CoreWebView2.Navigate("file:///"+ absolutePath);
            timer1.Start();
        }
        private void PrintMoveByUrl()
        {
            if (qq.Count > 0)
            {
                object url = qq.Dequeue();
                WebPrint.CoreWebView2.ExecuteScriptAsync("Move('" + url.ToString() + "')");
            }
            else if (q.Count > 0)
            {
                object url = q.Dequeue();
                WebPrint.CoreWebView2.ExecuteScriptAsync("Move('" + url.ToString()+ "')");
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;  //取消该事件响应
            this.Hide(); //隐藏
        }
        /// <summary>
        /// 去除底色
        /// </summary>
        public  void RetColor()
        {
            WebPrint.CoreWebView2.ExecuteScriptAsync("RetColor()");
        }

        public void PrintMove(string url)
        {
            q.Enqueue(url);

        }
        public void FastPrintMove(string url)
        {
            qq.Enqueue(url);

        }
        /// <summary>
        /// 清空打印任务
        /// </summary>
        public void ClearPrint()
        {
            q.Clear();
            qq.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            PrintMoveByUrl();
        }
        /// <summary>
        /// 测试打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button1_Click(object sender, EventArgs e)
        {
          await  WebPrint.CoreWebView2.ExecuteScriptAsync("Move(" + textBox1.Text + ")");
        }
    }
}
