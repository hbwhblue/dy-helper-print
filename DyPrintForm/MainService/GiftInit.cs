﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DyPrintForm.MainService
{
    /// <summary>
    /// 礼物执行方法
    /// </summary>
    public class GiftInit
    {
        public static void MainInit(string Username,int Num,string GiftName,string picHead,Form main)
        {
            Main main1 = (Main)main;
            
            if (GiftName.Contains("小心心"))
            {
                for (int i = 0; i < Num; i++)
                {
                    main1.PrintMove(picHead);
                }
            }
            else if (GiftName.Contains("大啤酒"))
            {
                for (int i = 0; i < Num; i++)
                {
                    main1.FastPrintMove(picHead);
                }
            }

           
        }
    }
}
